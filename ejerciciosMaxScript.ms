
-- PRIMERA PARTE (Caja partida con lazo y pelota)

-- Generaci�n de burbujitas a lo largo de un spline, a partir de una esfera patr�n
-- Localizamos una bola ya creada mediante su identificador "bolita" y la almacenamos en la variable "miPatron" por comodidad
miPatron = $bolita
-- Le ponemos un material con color base (difuso) amarillo con gran transparencia (0:transparente, 100:opaco) 
miPatron.material = standardMaterial diffuse:yellow opacity:20
-- De la misma manera localizamos el spline que interseca
camino = $lazo
-- De nuevo para el terreno
terreno = $superficie
-- Vamos a generar trece burbujitas, almacenamos este n�mero en otra variable
numBurbujas = 13

-- Creamos una funci�n auxiliar que nos servir� para pintar sobre una superf�cie a partir de una v�a
function calculaRuta un_patron un_camino un_terreno =
(
	-- Hacemos que para cada iteraci�n el vector normal al terreno sea un n�mero aleatorio entre
	-- cero y uno en flotante, calculamos la posici�n del vector y su tangente
	-- adem�s de la separaci�n con respecto a la ruta (desplazamiento)
	-- calculamos la intersecci�n del "rayo" del camino con respecto a la superf�cie
	-- lo cual nos dar� una posici�n donde colocar el nuevo objeto
	-- mientras que la escala la variamos dentro de l�mites peque�os aleatorios
	local normal = random 0.0 1.0,
		posicion = lengthInterp un_camino 1 normal,
		tangente = lengthTangent un_camino 1 normal,
		desplazamiento = random 1 4,	
		separacionVector = cross tangente z_axis * desplazamiento,
		separacionPosicion = posicion + separacionVector,
		ruta = ray [separacionPosicion.x,separacionPosicion.y,1000] [0,0,-1],
		nuevaPos = (intersectRay un_terreno ruta).pos,
		nuevaEscala = [1,1,1] * random 0.1 0.4
		
		-- Creamos pues el objeto copia de nuestro patr�n, en la posici�n "nuevaPos", con el tama�o "nuevaEscala" y de nombre "bola"
		instance un_patron pos:nuevaPos scale:nuevaEscala name:"bola"
)

-- La generaci�n se har� a trav�s de un bucle for desde la primera hasta la treceava burbuja a generar
-- En "burbuja_i" tendremos el contador de cada iteraci�n
-- NOTA: En todas las funciones, nos protegemos ante errores mediante avisos que notifiquen al usuario
try
(
	for burbuja_i in 1 to numBurbujas do
	(	
		calculaRuta mipatron camino terreno
	)
)
catch
(
	messageBox "Ha habido un error al generar las burbujas sobre la caja."
)


-- SEGUNDA PARTE (Carreteras curva y con picos)

-- Creamos una funci�n auxiliar que nos servir� para pintar objetos de tipo pir�mide
-- a lo largo de un camino, alternando su orientaci�n (+/-90�) mediante un flag (sem�foro verde/rojo)
-- con un radio tambi�n pasado por par�metro
fn calculaRutaGira un_camino un_radio gira =
(
	local normal = random 0.0 1.0,
		nuevoObjeto = pyramid width:un_radio depth:un_radio heigth:10.0  wireColor:blue
	
	nuevoObjeto.pos = lengthInterp un_camino 1 normal
	nuevoObjeto.dir = lengthTangent un_camino 1 normal
		
	coordsys local rotate nuevoObjeto 90 (if gira then x_axis else y_axis)
)

-- Ponemos un por defecto (lazo) por si no se escoge camino nuevo en el men� de a continuaci�n
nuevoCamino = camino

-- Creamos un men� emergente de dimensiones ancho300xalto300
rollout miMenu "Vamos a hacer un men�" width:300 height:300
(
	--Caja contenedora primer grupo con sus coordenadas prefijadas
	GroupBox contenedor1 "Primer clasificador" width:280 height:90
	label info "Elige un spline de la escena: " pos:[30,30] -- T�tulo del bot�n
	pickbutton pincha "[Escoge uno]" pos:[30,60] -- Bot�n para escoger un spline de la escena
	
	GroupBox contenedor2 "Segundo clasificador" width:280 height:60 pos:[13,100]
	spinner numInstancias "Cantidad: " type:#integer range:[1,10,10] pos:[30,130]
	--Rango a escoger de 1min a 100max con 10 de valor por defecto
	
	GroupBox contenedor3 "Tercer clasificador" width:280 height:120 pos:[13,165]
	slider barra "Rango" range: [0, 10, 30] pos:[20,190] -- Barra horizontal para escoger el radio de la figura
	button generar "Pincha para generar" pos:[30,245] -- Bot�n que lanza la creaci�n
	
	-- Cuando se pulse el bot�n de escoger, se asignar� el camino al spline elegido
	-- y el contenido del bot�n se sustituir� por su nombre
	on pincha picked escogido do
	(
		pincha.text = escogido.name
		nuevoCamino = escogido
	)
	-- Al pinchar el bot�n de generar llamaremos a nuestra funci�n auxiliar
	-- de giro, pas�ndole el camino y un flag cuyo valor cambiaremos por iteraci�n.
	on generar pressed do
	(
		try
		(
			bandera = false
			for objeto_i in 1 to numInstancias.value do
			(			
				calculaRutaGira nuevoCamino barra.value bandera
				bandera = not bandera
			)	
		)
		catch
		(
			messageBox "Ha habido un error al generar los objetos sobre el camino, por favor revise que haya escogido uno."
		)
	)
)
-- Lanzamos el di�logo para que se muestre, en coordenadas precisas.
createDialog miMenu pos:[500,100]


-- TERCERA PARTE (Renderizado)

-- Una vez hecha la interacci�n con la escena
-- pasamos a imprimir el resultado de cada una de las partes anteriores
-- lanzando un render desde la c�mara que se ha colocado en cada uno de ellos
-- para despu�s guardar la imagen bajo un directorio y unas dimensiones

-- "imprime" nombre de la funci�n, "size" tama�o por defecto aparte del que se le pasa
-- "folder" ruta donde se va a guardar, crear carpeta si no existe previamente
function imprimir size:[200,100] frames: folder:"c:/guarda/" = 
(
	-- itera sobre las c�maras existentes en la escena
	for camara_i in cameras do 
	(
		-- renderizamos la c�mara actual sobre la ruta m�s el nombre, en formato .jpg y con el tama�o pasado
		render camera:camara_i outputFile:(folder+camara_i.name+".jpg") outputSize:size		
   )
)

rollout miImpresora "Vamos a imprimir" width:300 height:150
(
	GroupBox contenedor "Dimensiones de la imagen" width:280 height:120
	spinner ancho "Ancho: " type:#integer range:[100,600,300] pos:[60,30]
	spinner alto "Alto: " type:#integer range:[100,600,200] pos:[75,60]
	button imprime "Pincha para imprimir" pos:[90,90]
	
	on imprime pressed do
	(
		try
		(
			-- Le pasamos a la funci�n de imprmir el ancho y alto elegidos de los rangos
			imprimir size:[ancho.value,alto.value]	
		)
		catch
		(
			messageBox "Ha habido un error al renderizar/guardar las im�genes."
		)
	)
)
createDialog miImpresora pos:[100,100]


-- CUARTA PARTE (Animaci�n)

-- Creamos de nuevo un men�, pero esta vez s�lo con un bot�n
-- Cuando se pulse crearemos una tetera la cual se transformar�
-- desde el frame 0 al 100, escal�ndose primero, cambi�ndose de posici�n segundo
-- y casi al final de la animaci�n ambas transformaciones
rollout miAnimacion "Vamos a animar" width:300 height:70
(
	GroupBox cajita "Activa con el bot�n:" width:280 height:60
	button play "�En marcha!" pos:[90,30]
	
	on play pressed do
	(
		try
		(
			miTetera = teapot pos:[0,0,0] wireColor:white
			animate on 
			( 
				at time 0 (miTetera.scale = [1,1,0.8])
				at time 50 (miTetera.pos = [100,100,100])
				at time 100 (miTetera.pos = [-50,-20,0]; miTetera.scale = [0.2,0.2,0.3])
			)	
		)
		catch
		(
			messageBox "Ha habido un error al animar la tetera."
		)
	)
)
createDialog miAnimacion pos:[100,300]